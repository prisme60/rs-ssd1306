use crate::Run;
use embedded_graphics::Drawing;
use embedded_graphics_simulator::{Display, DisplayBuilder, DisplayTheme, SimPixelColor};

impl Run for Display {
    fn run(&mut self) -> bool {
        self.run_once()
    }
}

pub fn init() -> impl Drawing<SimPixelColor> + Run {
    DisplayBuilder::new()
        .theme(DisplayTheme::OledBlue)
        .size(128, 64)
        .build()
}
