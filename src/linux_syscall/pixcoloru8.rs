use embedded_graphics::pixelcolor::PixelColor;
// `Copy` and `Clone` are bounds on `PixelColor` and are required.
// `PartialEq` is for the `assert_eq!()` later in this example.
// `Debug` is for convenience :)
#[derive(Copy, Clone, PartialEq, Debug)]
pub struct CustomPixelColor {
    pub value: u8,
}

impl CustomPixelColor {
    pub fn new(color: u8) -> Self {
        CustomPixelColor { value: color }
    }
}

impl PixelColor for CustomPixelColor {}

// `From<u8>` is a bound on `PixelColor` so must be implemented for your pixel colour type. You
// can also implement `From` for other types like `u16`, etc for convenience.
impl From<u8> for CustomPixelColor {
    fn from(other: u8) -> Self {
        CustomPixelColor { value: other }
    }
}
