use embedded_graphics::{drawable::Pixel, Drawing};
use std::fs;

mod pixcoloru8;

use self::pixcoloru8::CustomPixelColor;
use crate::Run;

/// Create a new window with a simulated display to draw into
///
/// You should use [`DisplayBuilder`] to create an instance of `Display`
///
/// [`DisplayBuilder`]: ./display_builder/struct.DisplayBuilder.html
pub struct Display<'a> {
    width: usize,
    height: usize,
    pixels: Box<[u8]>,
    fbdev: &'a str,
}

impl<'a> Display<'a> {
    pub fn new(width: usize, height: usize, fbdev: &'a str) -> Self {
        let pixels = vec![0u8; width * height / 8];

        Display {
            width,
            height,
            pixels: pixels.into_boxed_slice(),
            fbdev,
        }
    }

    pub fn clear(&self) {
        let empty_buff = vec![0u8; self.width * self.height / 8];
        fs::write(&self.fbdev, &empty_buff).expect("unable to clear to device");
    }
}

impl<'a> Run for Display<'a> {
    /// Update the display to show drawn pixels
    fn run(&mut self) -> bool {
        // copy temp buffer to device

        //self.clear();
        fs::write(&self.fbdev, &self.pixels).expect("unable to write to device");

        false
    }
}

impl<'a> Drawing<CustomPixelColor> for Display<'a> {
    fn draw<T>(&mut self, item_pixels: T)
    where
        T: IntoIterator<Item = Pixel<CustomPixelColor>>,
    {
        for Pixel(coord, color) in item_pixels {
            let x = coord[0] as usize;
            let y = coord[1] as usize;

            if x >= self.width || y >= self.height {
                continue;
            }

            let index = (y + 1) * self.width - (x + 1);
            let bit_to_set = 1 << ((index % 8) as u8);
            if color.value == 0 {
                self.pixels[index / 8] &= !bit_to_set; // Clear bit
            } else {
                self.pixels[index / 8] |= bit_to_set; // Set bit
            }
        }
    }
}

pub fn init<'a>(fbdev: &'a str) -> impl Drawing<CustomPixelColor> + Run + 'a {
    Display::new(128, 64, &fbdev)
}
