use crate::mqtt::TypeVal::{Bool, Float, Int, None};
use embedded_graphic_objects::{analog_clock, digital_clock};
use embedded_graphics::{
    egcircle, egrectangle, icoord, prelude::*, text_12x16, text_6x12, text_6x8, text_8x16,
    transform::Transform, Drawing,
};
use std::time::{Duration, Instant};

static CIRCLE_SIZE: usize = 32;

static TIME_REFRESH_MS: u64 = 500u64;

#[cfg(feature = "simulator")]
pub mod simulator;

#[cfg(not(feature = "simulator"))]
pub mod linux_syscall;

pub mod mqtt;

pub trait Run {
    fn run(&mut self) -> bool;
}

fn main() {
    #[cfg(feature = "simulator")]
    let mut display = simulator::init();

    #[cfg(not(feature = "simulator"))]
    let mut display = linux_syscall::init("/dev/fb1");
    let mut map = mqtt::MapTopicValue::new();
    let topics = [
        "stat/sonoff4ch/POWER1",
        "stat/sonoff4ch/POWER2",
        "stat/sonoff4ch/POWER3",
        "stat/sonoff4ch/POWER4",
    ];
    for topic in &topics {
        map.insert(topic, Bool(false));
    }

    let topics_f = [
        "alpine/htu_temp",
        "alpine/htu_hum",
        "alpine/bmp_temp",
        "alpine/bmp_press",
        "alpine/cpu_temp",
    ];
    //alpine/htu_temp 24.407
    //alpine/htu_hum 51.500
    //alpine/bmp_temp 25.130
    //alpine/bmp_press 998.49062500
    //alpine/cpu_temp 47.078

    for topic in &topics_f {
        map.insert(topic, Float(0f32));
    }

    let mut mqtt_client = mqtt::Mqtt::new("mqtt.toml", map);

    let r = egrectangle!(
        (0, 0),
        (128, 64),
        stroke = Some(0u8.into()),
        fill = Some(0u8.into())
    );

    let c_off = egcircle!(
        (16, 16),
        16,
        stroke = Some(1u8.into()),
        fill = Some(0u8.into())
    );

    let c_on = egcircle!(
        (16, 16),
        16,
        stroke = Some(1u8.into()),
        fill = Some(1u8.into())
    );

    let mut ana_clock =
        analog_clock::AnalogClock::new(CIRCLE_SIZE, (CIRCLE_SIZE, CIRCLE_SIZE, CIRCLE_SIZE / 2), 6);
    ana_clock.translate_mut(icoord!(96, CIRCLE_SIZE as i32));

    let digital_clock =
        digital_clock::DigitalClock::new(digital_clock::TextSize::Size12x16, (0, 48));

    loop {
        let mut ref_time = Instant::now();
        for hour in 0..12 {
            for minute in 0..60 {
                for second in 0..60 {
                    for flick in 0..2 {
                        display.draw(r);

                        if second % 10 > 5 {
                            for (i, topic_f) in topics_f.iter().enumerate() {
                                if let Float(f) = mqtt_client.get_value(topic_f) {
                                    display.draw(
                                        text_6x8!(&f.to_string())
                                            .translate(icoord!(0, 10 * i as i32)),
                                    );
                                }
                            }

                            ana_clock.set_time(hour, minute, second);
                            display.draw(&ana_clock);
                        } else {
                            let mut offset_h = 0;
                            for topic in &topics {
                                if let Bool(b) = mqtt_client.get_value(topic) {
                                    display.draw(
                                        if b { c_on } else { c_off }
                                            .translate(icoord!(offset_h, 0)),
                                    );
                                }
                                offset_h += 32;
                            }

                            digital_clock.draw(&mut display, hour, minute, second, flick > 0);
                        }

                        let end = display.run();
                        mqtt_client.update(Duration::from_millis(dbg!(
                            TIME_REFRESH_MS - (Instant::now() - ref_time).as_millis() as u64
                        )));
                        ref_time = Instant::now();

                        if end {
                            return;
                        }
                    }
                }
            }
        }
    }
}
