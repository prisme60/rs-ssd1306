use serde_derive::*;
use std::{fs::File, io::Read};
use toml;

#[derive(Deserialize, Debug)]
pub struct TBroker {
    pub server: String,
    pub port: u16,
    pub login: String,
    pub password: String,
}

pub fn load(broker_file_str: &str) -> TBroker {
    let mut broker_file = match File::open(broker_file_str) {
        Ok(f) => f,
        Err(e) => panic!(
            "Error occurred opening file: {} - Err: {}",
            broker_file_str, e
        ),
    };

    let mut broker_str = String::new();
    match broker_file.read_to_string(&mut broker_str) {
        Ok(s) => s,
        Err(e) => panic!("Error Reading file: {}", e),
    };
    println!("Zone File: {}", broker_file_str);

    let tml: TBroker = toml::from_str(&broker_str).unwrap();
    println!("Tml : {:?}", tml);
    tml
}
