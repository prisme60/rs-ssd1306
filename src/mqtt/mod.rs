mod conf;

use crossbeam_channel::select;
use rumqtt::{MqttClient, MqttOptions, Notification, QoS, Receiver};
use std::{collections::HashMap, time::Duration};

use crate::mqtt::TypeVal::{Bool, Float, Int, None};

#[derive(Copy, Clone)]
pub enum TypeVal {
    Bool(bool),
    Int(i32),
    Float(f32),
    None,
}

pub type MapTopicValue<'a> = HashMap<&'a str, TypeVal>;

pub struct Mqtt<'a> {
    mqtt_client: MqttClient,
    notifications: Receiver<Notification>,
    map_topics_values: MapTopicValue<'a>,
}

impl<'a> Mqtt<'a> {
    pub fn new(broker_file: &str, map_topics_values: MapTopicValue<'a>) -> Self {
        let tml = conf::load(broker_file);
        let mqtt_options =
            MqttOptions::new("test-sub", tml.server, tml.port).set_clean_session(false);
        let (mqtt_client, notifications) = MqttClient::start(mqtt_options).unwrap();
        let mut mqtt_struct = Self {
            mqtt_client,
            notifications,
            map_topics_values,
        };

        for &topic in mqtt_struct.map_topics_values.keys() {
            mqtt_struct
                .mqtt_client
                .subscribe(topic, QoS::AtLeastOnce)
                .unwrap();
        }

        mqtt_struct
    }

    pub fn update(&mut self, sleep_duration: Duration) {
        //println!("Begin mqtt");
        // select between mqtt notifications and other channel rx
        loop {
            select! {
                recv(self.notifications) -> notification => {
                    println!("{:?}", notification);
                    if let Ok(Notification::Publish(publish)) = notification
                    {
                        //if let Some((&k, &v)) = self.map_topics_values.get_key_value(&publish.topic_name)
                        if let Ok(val) = String::from_utf8(publish.payload.to_vec()) {
                            // The following line gives me a headache! https://stackoverflow.com/a/37521390
                            if let Some(var) = self.map_topics_values.get_mut::<str>(&publish.topic_name) {
                                *var = match *var {
                                    Bool(_) => Bool(match val.as_ref()
                                    {
                                        "ON" => true,
                                        "true" => true,
                                        _ => false
                                    }),
                                    Int(_) => Int(val.parse::<i32>().unwrap_or(0)),
                                    Float(_) => Float(val.parse::<f32>().unwrap_or(0f32)),
                                    None => None
                                }
                            }
                        }
                    }
                },
                default(sleep_duration) => break,
            }
            //println!("Loop");
        }
        //println!("End mqtt");
    }

    pub fn get_value(&self, topic: &str) -> TypeVal {
        if let Some(&type_val) = self.map_topics_values.get(topic) {
            type_val
        } else {
            None
        }
    }
}
