# rs-ssd1306

## Release compilation 

```bash
cargo build --release
```

If you want to run the program, you need to have ssd1306 hardware and the device should be present as **/dev/fb1** (configured by device tree).

## Simulator

### Screenshots of simulator

* "stat/sonoff4ch/POWER1" OFF
* "stat/sonoff4ch/POWER2" OFF
* "stat/sonoff4ch/POWER3" OFF
* "stat/sonoff4ch/POWER4" OFF


![alt text1](img/all_off.png "All indicators are OFF.")


* "stat/sonoff4ch/POWER4" ON 

![alt text1](img/last_on.png "All indicators are OFF except last indicator.")

### Release compilation

```bash
cargo build --release --features simulator
```

### Release execution
```bash
cargo run --release --features simulator
```